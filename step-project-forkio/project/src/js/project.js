
const burgerMenu = document.querySelector('.wrapper-burger-menu');
const burgerIcon = document.querySelector('.burger-icon');
const closeIcon = document.querySelector('.close-icon');
const menuIcons = document.querySelector('.menu-icons');

function toggleMenu (){
    burgerMenu.classList.toggle('show');
    burgerIcon.classList.toggle('hide');
    closeIcon.classList.toggle('show');
    
}

menuIcons.addEventListener('click', function(e) {
    e.stopPropagation();
    toggleMenu();
});

document.addEventListener('click', function(e) {
    const its_menu = e.target == burgerMenu || burgerMenu.contains(e.target);
    const its_btnMenu = e.target == burgerIcon;
    const menu_is_active = burgerMenu.classList.contains('show');
    
    if (!its_menu && !its_btnMenu && menu_is_active) {
        toggleMenu();
    }
});



const tabsTitle = document.querySelectorAll('.burger-menu-item')


tabsTitle.forEach(el => {
    el.addEventListener('click', (event) => {
       switchTab(event.target)
    })

})
function switchTab(tab){
    tabsTitle.forEach((item) => {
        item.classList.remove('active');
        if (item === tab){
            item.classList.add('active');
             
        }

    })
    
}
