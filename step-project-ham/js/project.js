let tab = function(){
    const tabsTitle = document.querySelectorAll('.title');
    const tabsContent = document.querySelectorAll('.content');
    
    tabsTitle.forEach(el => {
        el.addEventListener('click', (event) => {
            let currentIndex = switchTab(event.target);
            switchContent(currentIndex);
        })
    
    })
    function switchTab(tab){
        tabsTitle.forEach((item, index) => {
            item.classList.remove('title-active');
            if (item === tab){
                item.classList.add('title-active');
                 currentIndex = index;
            }
    
        })
        return currentIndex;
    }
    function switchContent(currentIndex){
        tabsContent.forEach((item, index) => {
            item.classList.remove('active-content');
            if (index === currentIndex){
                item.classList.add('active-content');
                
            }
        })
    }
}

tab();


function loadMoImg(){
    const btn = document.querySelector('.load-btn');
    const imgBlock = document.querySelector('.load-mo');
    imgBlock.style.display = 'grid';
    btn.style.display = 'none';
}


filterImg('all')
function filterImg(value){
    let arrImgs = document.querySelectorAll('.filtr-img');
    if(value == 'all') {
        arrImgs.forEach(el => el.classList.add('show-filter-imgs'))      
    } else {
        arrImgs.forEach(el => {
            el.classList.remove('show-filter-imgs')
            if(el.classList.contains(value)) {
                el.classList.add('show-filter-imgs')
            }
        })
    }
}


let btns = document.querySelectorAll('.section-five-menu-item')
for (let i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function() {
        let current = document.getElementsByClassName("active");
        current[0].className = current[0].className.replace(" active", "");
        this.className += " active";
    });
  }


  $(document).ready(function(){
    $('.sliderBig').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider'
    });
    $('.slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.sliderBig',
        dots: false,
        centerMode: true,
        focusOnSelect: true,
    });
   
});