// Теоретический вопрос
// Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript
// Например, у нас есть объект user со своими свойствами и методами, и мы хотим создать объекты admin
// и guest как его слегка изменённые варианты. Мы хотели бы повторно использовать то, что есть у объекта user,
// не копировать/переопределять его методы, а просто создать новый объект на его основе.
// Прототипное наследование — это возможность языка, которая помогает в этом.

// Задание

// Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата).
//  Сделайте так, чтобы эти свойства заполнялись при создании объекта.
// Создайте геттеры и сеттеры для этих свойств.
// Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).
// Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженное на 3.
// Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.

class Employee {
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get nameInfo(){
        return this.name; 
    }
    get ageInfo(){
       return this.age;
    }
    get salaryInfo(){
        return this.salary;
    }
    set nameInfo(newName){
        this.name = newName;
    }

    set ageInfo(newAge){
        this.age = newAge;
    }
    set salaryInfo(newSalary){
        this.salary = newSalary;
    }
}


class Programmer extends Employee{
    constructor(name, age, salary, lang ){
        super(name, age, salary);
        this.lang = lang;
    }
    get salaryInfo(){
        return this.salary * 3;
    }
    
}
const employee = new Employee('Max', 30, 200_000);
console.log({employee});

const programmer = new Programmer('Roma',25,300_000,['php','js', 'react']);
console.log({programmer});
console.log(programmer.salaryInfo);
