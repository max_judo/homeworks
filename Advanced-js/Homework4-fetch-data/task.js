// Теоретический вопрос
// Обьясните своими словами, что такое AJAX и чем он полезен при разработке на Javascript.

// AJAX это Асинхронный JavaScript и XML. Это набор методов веб-разработки, которые позволяют
//  веб-приложениям работать асинхронно — обрабатывать любые запросы к серверу в фоновом режиме.
//  И JavaScript, и XML работают асинхронно в AJAX. В результате любое веб-приложение, использующее AJAX,
//   может отправлять и извлекать данные с сервера без необходимости перезагрузки всей страницы.

// Задание
// Получить список фильмов серии Звездные войны, и вывести на экран список персонажей для каждого из них.

// Технические требования:

// Отправить AJAX запрос по адресу https://swapi.dev/api/films/ и получить список всех фильмов серии Звездные войны

// Для каждого фильма получить с сервера список персонажей, которые были показаны в данном фильме. 
// Список персонажей можно получить из свойства characters.
// Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на экран. 
// Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля episode_id, title и opening_crawl).
// Как только с сервера будет получена информация о персонажах какого-либо фильма, вывести эту информацию на экран под названием фильма.


// Необязательное задание продвинутой сложности

// Пока загружаются персонажи фильма, прокручивать под именем фильма анимацию загрузки. Анимацию можно использовать любую.
// Желательно найти вариант на чистом CSS без использования JavaScript.


// fetch('https://swapi.dev/api/films/')
// .then(response => response.json())
// .then(data => {
//     data.results.forEach(films => {
//         document.write(`<div>
//             <p>${films.episode_id}</p>
//             <h2>${films.title}</h2>
//             <p>${films.opening_crawl}</p>
//             <div class=id${films.episode_id}></div>
//         </div>`)
//         films.characters.forEach(character => {
//             fetch(character)
//             .then(response => response.json())
//             .then(data => {
//                 const film = document.querySelector(`.id${films.episode_id}`)
//                 film.innerHTML += `<p>${data.name}</p>`
//             })
//         })
//     })  
// })

function getFilms(){

 let data = fetch('https://swapi.dev/api/films/')
    .then(response => response.json())
    .then(data =>  data.results)
    return data
}


function getCharacters(){
    let films = getFilms()
    films.then(data => data.forEach(film => {
        addPage(film)
        film.characters.forEach(character => {
            fetch(character)
            .then(response => response.json())
            .then(data => {
                const dfilm = document.querySelector(`.id${film.episode_id}`)
                dfilm.innerHTML += `<p>${data.name}</p>`
            })
        })
        
    }))
}
function addPage(film){
   
        document.write(`<div>
        <p>${film.episode_id}</p>
        <h2>${film.title}</h2>
        <p>${film.opening_crawl}</p>
        <div class=id${film.episode_id}></div>
    </div>`)
  
   
        
}
getCharacters()