// Задание

// Создать страницу, имитирующую ленту новостей социальной сети Twitter.


// Технические требования:

// При открытии страницы, получить с сервера список всех пользователей и общий список публикаций.
//  Для этого нужно отправить GET запрос на следующие два адреса:

// https://jsonplaceholder.typicode.com/users
// https://jsonplaceholder.typicode.com/posts


// После загрузки всех пользователей и их публикаций, отобразить все публикации на странице.
// Каждая публикация должна быть отображена в виде карточки (пример: https://prnt.sc/q2em0x), 
// и включать заголовок, текст, а также имя, фамилию и имейл пользователя, который ее разместил.
// Для каждой карточки должен быть функционал ее редактирования и удаления.
// После редактирования карточки необходимо отправить PUT запрос по адресу https://jsonplaceholder.typicode.com/posts/${postId}.
// Для удаления карточки необходимо отправить DELETE запрос на https://jsonplaceholder.typicode.com/posts/${postId}.
// Вверху страницы должна быть кнопка Добавить публикацию. При нажатии на кнопку, открывать модальное окно, в котором пользователь
//  сможет ввести заголовок и текст публикации. После создания публикации данные о ней необходимо отправить в POST запросе по адресу:
//   https://jsonplaceholder.typicode.com/posts. Новая публикация должна быть добавлена вверху страницы
//    (сортировка в обратном хронологическом порядке).
//    В качестве автора присвоить публикации пользователя с id: 1.
// Более детальную информацию по использованию кжадого из этих API можно найти здесь.
// Данный сервер (jsonplaceholder.typicode.com) является тестовым. После перезагрузки страницы все изменения,
//  которые отправлялись на сервер, не будут там сохранены. Это нормально.


// Необязательное задание продвинутой сложности

// Пока с сервера при открытии страницы загружается информация, показывать анимацию загрузки. Анимацию можно использовать любую.
//  Желательно найти вариант на чистом CSS без использования JavaScript.
// const getUser = userId => {
//     return fetch(`https://jsonplaceholder.typicode.com/users/${userId}`, {
//         method: 'GET'
//     })
//         .then(response => {
//             if (response.ok) {
//                 return response.json()
//             } else {
//                 return new Error('error');
//             }
//         })
//         .catch(err => console.error(err))
// }

// const getPost = postId => {
//     return fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
//         method: 'GET'
//     })
//         .then(response => {
//             if (response.ok) {
//                 return response.json()
//             } else {
//                 return new Error('error');
//             }
//         })
//         .catch(err => console.error(err))
// }
fetch('https://jsonplaceholder.typicode.com/users')
  .then((response) => response.json())
  .then((json) => console.log(json));

fetch(' https://jsonplaceholder.typicode.com/posts')
  .then((response) => response.json())
  .then((json) => console.log(json));




//   function renderCard(){
   
//     document.write(`<div>
        // const img = new Image();
        // img.src ='./img/2png'
//     <p>${film.episode_id}</p>
//     <h2>${film.title}</h2>
//     <p>${film.opening_crawl}</p>
//     <div class=id${film.episode_id}></div>
// </div>`)


    
// }
//   const renderUser = ({ id, name, username,email}) => {
//     userEl.insertAdjacentHTML('beforeend', `<p>${id}</p>`);
//     userEl.insertAdjacentHTML('beforeend', `<p>${name}</p>`);
//     userEl.insertAdjacentHTML('beforeend', `<p>${username}</p>`);
//     userEl.insertAdjacentHTML('beforeend', `<p><a href="mailto:${email}">${email}</a></p>`);
//     userEl.insertAdjacentHTML('beforeend', `<p><a target="_blank" href="${website}">${website}</a></p>`);
// }

// const renderPost = ({ name, email, website }) => {
  
//     userEl.insertAdjacentHTML('beforeend', `<p>${name}</p>`);
//     userEl.insertAdjacentHTML('beforeend', `<p><a href="mailto:${email}">${email}</a></p>`);
//     userEl.insertAdjacentHTML('beforeend', `<p><a target="_blank" href="${website}">${website}</a></p>`);
// }
 

class UserCard {
    constructor(name, lastname, email, title, text) {
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.title = title;
        this.text = text;
    }

    
}