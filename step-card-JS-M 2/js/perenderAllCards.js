import { getCards } from './api.js';
import { AllCards } from './doctorsCard.js';

export const renderAllCards = async (filterCards) => {
    let cards
    if(filterCards) { cards = filterCards } 
    else { cards = await getCards() }
    const renderCards = new AllCards(cards);
    renderCards.render();
}