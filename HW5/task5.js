// Теоретический вопрос

// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
// Обычно языки программирования, текстовые командные интерфейсы, языки разметок текста (HTML) 
// имеют дело со структурированным текстом, в котором некоторые символы (и их комбинации) используются в качестве управляющих,
//  в том числе управляющих структурой текста. 
// В ситуации, когда необходимо использовать такой символ в качестве «обычного символа языка», применяют экранирование.

// Задание
// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:

// Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:

// При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
// Создать метод getAge() который будет возвращать сколько пользователю лет.
// Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).


// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.


function createNewUser() {
    let firstName = prompt('Enter your name');
    let lastName = prompt('Enter your last name');
    let birthday = prompt('Enter your birthday');
    let newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        getLogin: function () {

            return `${this.firstName[0]}${this.lastName}`.toLowerCase();
        },
        getAge: function () {
            let birthdayMill = new Date(this.birthday);
            let currentDay = new Date();
            let different = currentDay - birthdayMill;
            let oneYear = 1000 * 60 * 60 * 24 * 365;
            let age = Math.floor(different / oneYear);
            return age;
        },
        getPassword: function () {

            let firthLetter = this.firstName[0].toLocaleUpperCase();
            let lastNameLoverCase = this.lastName.toLocaleLowerCase();
            let birthYear = this.birthday.split('.')[2];
            return `${firthLetter}${lastNameLoverCase}${birthYear}`;
        }

    }
    return newUser
}
const resultCreateNewUser = createNewUser();
console.log(resultCreateNewUser.getLogin());
console.log(resultCreateNewUser.getAge());
console.log(resultCreateNewUser.getPassword());
