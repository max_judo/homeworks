// Теоретический вопрос

// Опишите своими словами, как Вы понимаете, что такое обработчик событий.
//Обработчик событий - это функция, которая обрабатывает, или откликается на событие.

// Задание
// Создать поле для ввода цены с валидацией. Задача должна быть реализована на языке javascript, без использования фреймворков 
//  и сторонник библиотек (типа Jquery).

// Технические требования:

// При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:

// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.

// Когда убран фокус с поля - его значение считывается, над полем создается span, 
// в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}.

// Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.

// При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.

// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, 
// под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
let parent = document.body

let input = document.createElement('input');
input.placeholder = 'Price';
parent.append(input);


input.addEventListener('focus', function () {
    input.style.border = '2px solid green';
    input.style.outline = 'none';
})

input.addEventListener('blur', function () {
    input.style.border = '2px solid black';
    input.style.color = 'black';
    if (input.value > 0) {
        input.style.color = 'green';
        let parentDiv = document.createElement('div');
        parent.prepend(parentDiv);
        let span = document.createElement('span');
        span.innerText = `Текущая цена: ${input.value}`
        parentDiv.prepend(span);
        let btn = document.createElement('button');
        btn.innerText = 'X';
        btn.addEventListener('click', function () {
            input.value = '';
            parentDiv.remove();
        })
        parentDiv.append(btn);


    } else if (input.value !== '') {
        let parentDivRed = document.createElement('div');
        parent.append(parentDivRed);

        input.style.border = '2px solid red';
        let redSpan = document.createElement('span');
        redSpan.innerText = 'Please enter correct price';
        parentDivRed.append(redSpan);

    }

})

