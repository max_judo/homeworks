import {GET_PRODUCTS, TOOGLE_MODAL_ADD_TO_BASKET, TOOGLE_MODAL_DELETE_FROM_BASKET} from './types'

const initalState = {
   isOpenModalAddToBasket: false,
   isOpenModalDeleteFromBasket: false,
};

const rootReducer = (state = initalState, action) => {
    switch (action.type) {
        case GET_PRODUCTS:
            return {
                ...state,
                products: action.payload
            }
        case TOOGLE_MODAL_ADD_TO_BASKET:
            return{
                ...state,
                isOpenModalAddToBasket: !state.isOpenModalAddToBasket
            }    
        case TOOGLE_MODAL_DELETE_FROM_BASKET:
            return{
                ...state,
                isOpenModalDeleteFromBasket: !state.isOpenModalDeleteFromBasket
            }    
        default: 
            return state
    }
}

export default rootReducer