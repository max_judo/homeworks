import {GET_PRODUCTS, TOOGLE_MODAL_ADD_TO_BASKET, TOOGLE_MODAL_DELETE_FROM_BASKET} from './types' ;


const actionGetProduct = (data) => ({
    type: GET_PRODUCTS,
    payload: data
})


const getProducts = () => dispatch => {
   return fetch('products.json')
    .then(response => response.json())
    .then(resolve => dispatch(actionGetProduct(resolve)))
}

 const toggleModalAddToBasket = () => dispatch => {
   return dispatch({
        type: TOOGLE_MODAL_ADD_TO_BASKET
    })
 }

 const toggleModalDeleteFromBasket = () => dispatch => {
    return dispatch({
         type: TOOGLE_MODAL_DELETE_FROM_BASKET
     })
  }
 

export {getProducts, toggleModalAddToBasket, toggleModalDeleteFromBasket}



