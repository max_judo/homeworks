import React from 'react'
import "./Button.scss"
import PropTypes from 'prop-types'

const Button = (props) => {
    const {backgroundColor, text, functionClick} = props;
        return (
           <button onClick={functionClick} style={{backgroundColor:backgroundColor}} className="btn"> 
               {text}   
           </button>
        )

}
export default Button
Button.propTypes ={
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    functionClick: PropTypes.func
}