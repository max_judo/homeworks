import React, {useState} from 'react';
import Product from '../allProducts/Product';
import {ReactComponent as CloseIcon} from "../../assets/svg/close.svg";
import Modal from "../../Component/Modal/Modal";
import Button from "../../Component/Button/Button";
import './Basket.scss';
import {useDispatch, useSelector} from 'react-redux';
import {toggleModalDeleteFromBasket} from '../../store/actions'


const Basket = () => {
    const  dispatch = useDispatch()
    const isShowModal = useSelector(state => state.isOpenModalDeleteFromBasket)

    const[cardArr, setCardArr] = useState(JSON.parse(localStorage.getItem('cardArr')) )
    const[currentCardId, setCurrentCardId] = useState(null)
    

    const showModal = (id) => {
        dispatch(toggleModalDeleteFromBasket())
        setCurrentCardId(id)
    }
    const hideModal = () => {
        dispatch(toggleModalDeleteFromBasket())
        setCurrentCardId(null)
    }

    const deleteCard = (id) =>{
        const newCard = cardArr.filter(card => card.id !== id)
        setCardArr(newCard)
        localStorage.setItem('cardArr', JSON.stringify(newCard))
        hideModal()
    }

    const footertModal =
    <div>
       <Button text="OK" backgroundColor={"#a4261d"} functionClick={() => deleteCard(currentCardId)}/>
       <Button text="Cancel" backgroundColor={"#a4261d"} functionClick={hideModal}/>  
    </div>

return (

    <div className="basket">
    {cardArr && cardArr.length !== 0 ?
        cardArr.map(product =>(
            <div className="wrap-product">
                <div className="delete-btn" onClick={() =>showModal(product.id)}>
                    <CloseIcon className="close-icon" />
                </div>
                <Product productInfo={product}/> 
            </div>
        
        ))
        :
        "Basket empty"
        }
       {isShowModal &&
            <Modal
                functionClose={hideModal}
                header={"Delete card"}
                closeButton={true}
                textContent={"Are you sure?"} 
                footer={ footertModal }
            />
        }
    </div>
)
}
export default Basket