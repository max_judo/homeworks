import React, { useState } from 'react'
import "./Product.scss"
import Button from "../../Component/Button/Button"
import Modal from "../../Component/Modal/Modal"
import {ReactComponent as StarIconFirst} from "../../assets/svg/star1.svg"
import {ReactComponent as StarIconSecond} from "../../assets/svg/star2.svg"
import PropTypes from 'prop-types'
import {useDispatch,useSelector} from 'react-redux'
import {toggleModalAddToBasket} from '../../store/actions'


 const Product = (props) => {
    const dispatch = useDispatch()
    const isShowModal = useSelector(state => state.isOpenModalAddToBasket)



    const {productInfo, changeSelectArr, isSelect, isShowButtonAddToBasket } = props;
    const {id, name, price, url, article, color} = productInfo;


    const addProductToCard = (productInfo) => {
        let newCardArr = JSON.parse(localStorage.getItem('cardArr')) || []
        newCardArr.push(productInfo)
       
        localStorage.setItem('cardArr',JSON.stringify(newCardArr))
        dispatch(toggleModalAddToBasket())
    }

        const footerModal =
   <div>
      <Button text="OK" backgroundColor={"#a4261d"} functionClick={() => addProductToCard(productInfo)}/>
      <Button text="Cancel" backgroundColor={"#a4261d"} functionClick={() => dispatch(toggleModalAddToBasket())}/>
    </div>
        return ( 
           
            <div className="product">
                <div className="product-content">
                <img className="product-img" src={url}/>
                <div className="product-name">{name}</div>
                <div className="product-prize">{price}</div>
                <div>{color}</div>
                <div className="product-article">{article}</div>
                <div  onClick={() => changeSelectArr(productInfo)}>{isSelect?<StarIconFirst className="star-icon"/>:<StarIconSecond className="star-icon"/>}</div>
                {isShowButtonAddToBasket &&
                    <Button functionClick={() => dispatch(toggleModalAddToBasket())}
                    text="Add to basket"
                    backgroundColor="#000"
                    />
                }
                {/* коли true(isShowModal)-показує,все що справа */}
                {isShowModal && <Modal 
                   functionClose={() => dispatch(toggleModalAddToBasket())} 
                    header="Add to basket"
                    closeButton={true}
                    textContent="Are you sure?"
                    footer={footerModal}
                   />}
                   </div>
            </div>
            
            
        )
    }
   
export default Product

Product.propTypes = {
    productInfo: PropTypes.object,
    changeSelectArr: PropTypes.func,
    isSelect: PropTypes.bool,
}