import React, { Component } from 'react'
import "./Button.css"
export default class Button extends Component {
    render() {
        const {backgroundColor, text, functionClick} = this.props;
        return (
           <button onClick={functionClick} style={{backgroundColor:backgroundColor}} className="btn"> 
               {text}   
           </button>
        )
    }

}
