import React, { Component } from 'react'
import "./Modal.css"
import {ReactComponent as CloseIcon} from "../../assets/svg/close.svg"
export default class Modal extends Component {
    render() {
        const {functionClose, header, closeButton, textContent, footer} = this.props;
        return (
            
           <div className="modal" onClick={functionClose}>
                <div className="modal-inner" onClick={(e) => e.stopPropagation()}>
                    <div className="modal-header">
                       {header} 
                       {closeButton && <CloseIcon className="close-svg" onClick={functionClose}/>}
                    </div>

                    <div className="modal-content">
                        {textContent}
                    </div>

                    <div className="modal-footer">
                        {footer}
                    </div>
                </div>
            </div>

        )
    }
}
