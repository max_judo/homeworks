import Button from './Button';
import { render, fireEvent} from '@testing-library/react'

describe('Button component', () => {
    it('should render correctly', () => {
        render(<Button/>)
    })

    it('should render correctly by name', () => {
        const testTitle = 'Add to cart'
        const { getByText } = render(<Button text={testTitle} />)
        getByText(testTitle)
    
    })

    it('should add class', () => {
        const className = 'btn';
        const { queryByRole } = render(<Button className={className}>Add to cart</Button>)
        const button = queryByRole('button')
        expect(button.className).toEqual(className)
    })


})