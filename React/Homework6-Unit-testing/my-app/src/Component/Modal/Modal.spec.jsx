import Modal from './Modal';
import { render} from '@testing-library/react'

describe('Modal component', () => {
    it('should render correctly', () => {
        render(<Modal/>)
    })

    
    it('should add textContent', () => {
        const textContent = 'test';
        const {getByText} = render(<Modal textContent={textContent} />)
        getByText(textContent)

    })

})