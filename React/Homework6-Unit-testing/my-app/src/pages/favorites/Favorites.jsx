import React, {useState} from 'react';
import Product from '../allProducts/Product';
import "./Favorites.scss"



const Favorites = () => {
    const [favoritesArr, setFavoritesArr] =  useState(JSON.parse(localStorage.getItem('selectArr'))) 
    const changeFavoritesArr = (productInfo) => {
       
        const newFavoritesArr = favoritesArr.filter(card => card.id !== productInfo.id)
        localStorage.setItem('selectArr', JSON.stringify(newFavoritesArr))
        setFavoritesArr(newFavoritesArr)
    }
return(
    <div className="favorites">
        {favoritesArr && favoritesArr.length !== 0 ?
        favoritesArr.map(product =>(
            <div >
                <Product
                     productInfo={product}
                    changeSelectArr={changeFavoritesArr}
                    isSelect={favoritesArr.findIndex(el => el.id === product.id) !== -1} 
                    isShowButtonAddToBasket={true}
                 /> 
            </div>
        
        ))
        :
        "Favorite empty"
        }
     
    </div>
)
}
export default Favorites