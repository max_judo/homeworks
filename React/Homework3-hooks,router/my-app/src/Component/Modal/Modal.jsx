import React from 'react'
import "./Modal.scss"
import {ReactComponent as CloseIcon} from "../../assets/svg/close.svg"
import PropTypes from 'prop-types'
const Modal = (props) => {
        const {functionClose, header, closeButton, textContent, footer} = props;
        return (
            
           <div className="modal" onClick={functionClose}>
                <div className="modal-inner" onClick={(e) => e.stopPropagation()}>
                    <div className="modal-header">
                       {header} 
                       {closeButton && <CloseIcon className="close-svg" onClick={functionClose}/>}
                    </div>

                    <div className="modal-content">
                        {textContent}
                    </div>

                    <div className="modal-footer">
                        {footer}
                    </div>
                </div>
            </div>

        )
    
}
export default Modal
Modal.propTypes = {
    functionClos: PropTypes.func,
    header: PropTypes.string,
    closeButton: PropTypes.func,
    textContent: PropTypes.string,
     footer: PropTypes.node
}