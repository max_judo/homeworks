import React from 'react'
import { Link } from 'react-router-dom'
import {ReactComponent as StarIconFirst} from "../assets/svg/star1.svg"
import {ReactComponent as BasketIcon} from "../assets/svg/basket.svg"
import {ReactComponent as Apple} from "../assets/svg/apple.svg"
import "./Header.scss"

export default function Header() {
    return (
        <div className="header">
            
            <div><Link to="/all-products"><Apple className="svg-header" /></Link></div>
            <div><Link to="/basket"><BasketIcon className="svg-header" /></Link>
            <Link to="/favorites"><StarIconFirst className="svg-header" /></Link></div>
            
        </div>
    )
}
