import React, { useState, useEffect } from 'react';
import './App.scss';
import AllProduct from './pages/allProducts/AllProduct';
import Favorites from './pages/favorites/Favorites';
import Basket from './pages/basket/Basket';
import {Switch, Route, Redirect} from 'react-router-dom';
import Header from './containers/Header'

const App = () => {

    return (  
      <>
      <Header />
        <Switch>
          <Redirect exact from='/' to='/all-products'/>
          <Route  path="/favorites">
            <Favorites />
          </Route>
          <Route  path="/basket">
            <Basket />
          </Route>
          <Route path="/all-products">
          <AllProduct />
            </Route>
        </Switch>
      </>
      
    );
  }
export default App;
