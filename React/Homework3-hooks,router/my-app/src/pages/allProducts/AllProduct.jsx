import React, { useState, useEffect } from 'react'
import Product from './Product'
import './AllProduct.scss'
import PropTypes from 'prop-types'

const AllProduct = () => {
    const [selectArr, setSelectArr] =  useState(JSON.parse(localStorage.getItem('selectArr')) || []) 
    const [products, setProducts] = useState(null)
    
    useEffect(() => {
      fetch('products.json')
      .then(response => response.json())
      .then(resolve => setProducts(resolve))
    }, [])
  
    
   const changeSelect = (productInfo) => {

        const index = selectArr.findIndex(el => el.id === productInfo.id);
        console.log(index);
        let newSelectArr = [...selectArr];
        if(index !== -1) {
            newSelectArr.splice(index,1)
        } else {  
            newSelectArr.push(productInfo)
        }
        setSelectArr(newSelectArr)
         localStorage.setItem('selectArr', JSON.stringify(newSelectArr))
    }
  
        return (
         <div className="products">
            {products && products.map(product =>(
                <Product 
                    productInfo={product}
                    changeSelectArr={changeSelect}
                    isSelect={selectArr.findIndex(el => el.id === product.id) !== -1} 
                    isShowButtonAddToBasket={true}
                />
            ))}
         </div>
          
        )
    }
    export default AllProduct

AllProduct.propTypes = {
    products: PropTypes.array
}