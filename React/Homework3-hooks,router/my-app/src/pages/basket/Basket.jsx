import React, {useState} from 'react';
import Product from '../allProducts/Product';
import {ReactComponent as CloseIcon} from "../../assets/svg/close.svg";
import Modal from "../../Component/Modal/Modal";
import Button from "../../Component/Button/Button";
import './Basket.scss'


const Basket = () => {
    const[cardArr, setCardArr] = useState(JSON.parse(localStorage.getItem('cardArr')) )
    const[currentCardId, setCurrentCardId] = useState(null)

    const deleteCard = (id) =>{
        const newCard = cardArr.filter(card => card.id !== id)
        setCardArr(newCard)
        localStorage.setItem('cardArr', JSON.stringify(newCard))
        setCurrentCardId(null)
    }

    const footertModal =
    <div>
       <Button text="OK" backgroundColor={"#a4261d"} functionClick={() => deleteCard(currentCardId)}/>
       <Button text="Cancel" backgroundColor={"#a4261d"} functionClick={() => setCurrentCardId(null)}/>
     </div>

return (

    <div className="basket">
    {cardArr && cardArr.length !== 0 ?
        cardArr.map(product =>(
            <div className="wrap-product">
                <div className="delete-btn" onClick={() =>setCurrentCardId(product.id)}>
                    <CloseIcon className="close-icon" />
                </div>
                <Product productInfo={product}/> 
            </div>
        
        ))
        :
        "Basket empty"
        }
       {currentCardId &&
            <Modal
                functionClose={() => setCurrentCardId(null)}
                header={"Delete card"}
                closeButton={true}
                textContent={"Are you sure?"} 
                footer={ footertModal }
            />
        }
    </div>
)
}
export default Basket