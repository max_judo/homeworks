import React, { useState } from 'react'
import "./Product.scss"
import Button from "../../Component/Button/Button"
import {ReactComponent as StarIconFirst} from "../../assets/svg/star1.svg"
import {ReactComponent as StarIconSecond} from "../../assets/svg/star2.svg"
import PropTypes from 'prop-types'
import {useDispatch} from 'react-redux'
import {toggleModalAddToBasket} from '../../store/actions'


 const Product = (props) => {
   const dispatch = useDispatch()
    const {productInfo, changeSelectArr, isSelect, isShowButtonAddToBasket, setCurrentProduct } = props;
    const {id, name, price, url, article, color} = productInfo;

        return ( 
           
            <div className="product">
                <div className="product-content">
                <img className="product-img" src={url}/>
                <div className="product-name">{name}</div>
                <div className="product-prize">{price}</div>
                <div>{color}</div>
                <div className="product-article">{article}</div>
                <div  onClick={() => changeSelectArr(productInfo)}>{isSelect?<StarIconFirst className="star-icon"/>:<StarIconSecond className="star-icon"/>}</div> 
                {isShowButtonAddToBasket &&
                    <Button functionClick={() => {
                        setCurrentProduct(productInfo)
                        dispatch(toggleModalAddToBasket())}}
                    text="Add to basket"
                    backgroundColor="#000"
                    />
                }
           
                   </div>
            </div>
            
            
        )
    }
   
export default Product

Product.propTypes = {
    productInfo: PropTypes.object,
    changeSelectArr: PropTypes.func,
    isSelect: PropTypes.bool,
}