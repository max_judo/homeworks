import React, { useState, useEffect } from 'react'
import Product from './Product'
import './AllProduct.scss'
import PropTypes from 'prop-types'
import {getProducts} from '../../store/actions'
import {useDispatch, useSelector} from 'react-redux'
import {toggleModalAddToBasket} from '../../store/actions'
import Modal from "../../Component/Modal/Modal"
import Button from "../../Component/Button/Button"
const AllProduct = () => {
    const [selectArr, setSelectArr] =  useState(JSON.parse(localStorage.getItem('selectArr')) || []) 
    const [currentProduct, setCurrentProduct] = useState(null)
    const isShowModal = useSelector(state => state.isOpenModalAddToBasket)
    const dispatch = useDispatch()

    useEffect(() => {
       dispatch(getProducts()) 
    }, [])
    const products = useSelector(state => state.products)

   const changeSelect = (productInfo) => {

        const index = selectArr.findIndex(el => el.id === productInfo.id);
        let newSelectArr = [...selectArr];
        if(index !== -1) {
            newSelectArr.splice(index,1)
        } else {  
            newSelectArr.push(productInfo)
        }
        setSelectArr(newSelectArr)
         localStorage.setItem('selectArr', JSON.stringify(newSelectArr))
    }
    const addProductToCard = () => {
        let newCardArr = JSON.parse(localStorage.getItem('cardArr')) || []
        newCardArr.push(currentProduct)
    
       console.log(newCardArr);
        localStorage.setItem('cardArr',JSON.stringify(newCardArr))

        dispatch(toggleModalAddToBasket())
    }
    const footerModal =
    <div>
       <Button text="OK" backgroundColor={"#a4261d"} functionClick={() => addProductToCard()}/>
       <Button text="Cancel" backgroundColor={"#a4261d"} functionClick={() => dispatch(toggleModalAddToBasket())}/>
     </div>
  
        return (
         <div className="products">
            {products && products.map((product, index) =>(
                <Product 
                    key = {index}
                    setCurrentProduct={setCurrentProduct}
                    productInfo={product}
                    changeSelectArr={changeSelect}
                    isSelect={selectArr.findIndex(el => el.id === product.id) !== -1} 
                    isShowButtonAddToBasket={true}
                />
            ))}
            {isShowModal && <Modal 
                   functionClose={() => dispatch(toggleModalAddToBasket())} 
                    header="Add to basket"
                    closeButton={true}
                    textContent="Are you sure?"
                    footer={footerModal}
                   />}
         </div>
          
        )
    }
    export default AllProduct

AllProduct.propTypes = {
    products: PropTypes.array
}