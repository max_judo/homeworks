import * as yup from "yup";
const phoneValidate = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
const BasicFormSchema = yup.object().shape({
    firstName: yup
    .string()
    .required('This field is required'),
    lastName: yup
    .string()
    .required('This field is required'),
    age: yup
    .number()
    .min(18, "Min age is 18 year")
    .required('This field is required'),
    phoneNumber: yup
    .string()
    .matches(phoneValidate, 'Enter correct number')
    .required('This field is required'),
    address: yup
    .string()
    .required('This field is required'),
});
export default BasicFormSchema;
