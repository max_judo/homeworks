import React from "react";
import {useDispatch} from 'react-redux';
import { Formik, Field, Form, ErrorMessage } from "formik";
import {setForm, clearStorage} from '../../store/actions'
import BasicFormSchema from "./BasicFormSchema";
import './checkoutForm.scss';

const CheckoutForm = (props) => {
    const dispatch = useDispatch()
    const {clearAllCard}=props;
    return(
    <div>
        <Formik
        initialValues={{ 
            firstName: '',
            lastName: '',
            age: '',
            address: '',
            phoneNumber: '', 
        }}
        validationSchema={BasicFormSchema}
        onSubmit={(values) => {
            dispatch(setForm(values));
            console.log(JSON.parse(localStorage.getItem('cardArr')));
            clearAllCard(null);
            dispatch(clearStorage());
            console.log(values)
        }}
        >
        {({
            handleSubmit
        }) =>  (
                <Form  onSubmit={handleSubmit}>
                <div className="main">
                   <div className="error-field">
                        <label className="input-form" htmlFor="firstName">firstName</label>
                        <Field type="text" name="firstName" placeholder="FirstName" />
                        <ErrorMessage name="firstName" />
                   </div>
                    <div className="error-field">
                        <label className="input-form" htmlFor="lastName">lastName</label>
                        <Field type="text" name="lastName" placeholder="LastName" />
                        <ErrorMessage name="lastName" />
                    </div>
                    <div className="error-field">
                        <label className="input-form" htmlFor="number">number</label>
                        <Field type="number" name="age" placeholder="Age" />
                        <ErrorMessage name="age" />
                    </div>
                    <div className="error-field">
                        <label className="input-form" htmlFor="address">address</label>
                        <Field type="text" name="address" placeholder="Address" />
                        <ErrorMessage name="address" />
                    </div>
                   <div className="error-field">
                        <label className="input-form" htmlFor="phoneNumber">phoneNumber</label>
                        <Field type="text" name="phoneNumber" placeholder="PhoneNumber" />
                        <ErrorMessage name="phoneNumber"/>
                   </div>
                   <button className="btn-form" type="submit">Checkout</button>
            </div>
                  
                </Form>
            ) 
        }
        </Formik>
    </div>
)}
    

export default CheckoutForm;