import {GET_PRODUCTS, TOOGLE_MODAL_ADD_TO_BASKET, TOOGLE_MODAL_DELETE_FROM_BASKET,SET_FORM_DATA,CLEAR_STORAGE} from './types'


const initalState = {
   isOpenModalAddToBasket: false,
   isOpenModalDeleteFromBasket: false,
   formData: {}
};

 const rootReducer = (state = initalState, action) => {
   
    switch (action.type) {
        case GET_PRODUCTS:
            return {
                ...state,
                products: action.payload
            }
        case TOOGLE_MODAL_ADD_TO_BASKET:
            return{
                ...state,
                isOpenModalAddToBasket: !state.isOpenModalAddToBasket
            }    
        case TOOGLE_MODAL_DELETE_FROM_BASKET:
            return{
                ...state,
                isOpenModalDeleteFromBasket: !state.isOpenModalDeleteFromBasket
            }        
        case SET_FORM_DATA:
                return {
                    formData: action.payload
                }
         case CLEAR_STORAGE:
                localStorage.removeItem('cardArr')   
        default: 
            return state
    }

}

export default rootReducer

