import React, { Component } from 'react'
import "./Button.scss"
import PropTypes from 'prop-types'

export default class Button extends Component {
    render() {
        const {backgroundColor, text, functionClick} = this.props;
        return (
           <button onClick={functionClick} style={{backgroundColor:backgroundColor}} className="btn"> 
               {text}   
           </button>
        )
    }

}
Button.propTypes ={
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    functionClick: PropTypes.func
}