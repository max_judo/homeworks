import React, { Component } from 'react'
import "./Product.scss"
import Button from "../Button/Button"
import Modal from "../Modal/Modal"
import {ReactComponent as StarIconFirst} from "../../assets/svg/star1.svg"
import {ReactComponent as StarIconSecond} from "../../assets/svg/star2.svg"
import PropTypes from 'prop-types'



export default class Product extends Component {
constructor() {
super()
this.state = {
    isShowModal: false, 
    cardArr: JSON.parse(localStorage.getItem('cardArr')) || []
}

}



showModal = () => {
    this.setState({isShowModal: true})
}
closeModal = () => {
    this.setState({isShowModal: false})
}
addProductToCard = (id) => {
    let newCardArr = JSON.parse(localStorage.getItem('cardArr')) || []
    newCardArr.push(id)
    console.log(newCardArr);
     localStorage.setItem('cardArr',JSON.stringify(newCardArr))
    
    this.closeModal()
}

    render() {
        const {productInfo, changeSelectArr, isSelect } = this.props;
        const {id, name, price, url, article, color} = productInfo;
       
        const {isShowModal} = this.state;
        const footerModal =
   <div>
      <Button text="OK" backgroundColor={"#a4261d"} functionClick={() => this.addProductToCard(id)}/>
      <Button text="Cancel" backgroundColor={"#a4261d"} functionClick={this.closeModal}/>
    </div>
        return ( 
           
            <div className="product">
                <div className="product-content">
                <img className="product-img" src={url}/>
                <div className="product-name">{name}</div>
                <div className="product-prize">{price}</div>
                <div>{color}</div>
                <div className="product-article">{article}</div>
                <div  onClick={() => changeSelectArr(id)}>{isSelect?<StarIconFirst className="star-icon"/>:<StarIconSecond className="star-icon"/>}</div>
                <Button functionClick={this.showModal}
                text="Add to card"
                backgroundColor="#000"
                />
                {isShowModal && <Modal 
                    functionClose={this.closeModal} 
                    header="Add to card"
                    closeButton={true}
                    textContent="Are you sure?"
                    footer={footerModal}
                   />}
                   </div>
            </div>
            
            
        )
    }
   
}

Product.propTypes = {
    productInfo: PropTypes.object,
    changeSelectArr: PropTypes.func,
    isSelect: PropTypes.bool,
}