import React, { Component } from 'react'
import Product from './Product'
import './AllProduct.scss'
import PropTypes from 'prop-types'

export default class AllProduct extends Component {
    state = {

        selectArr: JSON.parse(localStorage.getItem('selectArr')) || []
    }
    changeSelect = (id) => {
        const { selectArr } = this.state;
        const index = selectArr.indexOf(id);
        let newSelectArr = [...selectArr];
        if(index !== -1) {
            newSelectArr.splice(index,1)
        } else {       
            newSelectArr.push(id)
        }
        this.setState({selectArr: newSelectArr},
             () => localStorage.setItem('selectArr', JSON.stringify(newSelectArr)))
    }
    render() {
        
        const {products} = this.props
        const{ selectArr} = this.state
        return (
            
         <div className="products">
            {products && products.map(product =>(
                <Product 
                    productInfo={product}
                    changeSelectArr={this.changeSelect}
                    isSelect={selectArr.indexOf(product.id) !== -1} 
                />
            ))}
         </div>
          
        )
    }
}
AllProduct.propTypes = {
    products: PropTypes.array
}