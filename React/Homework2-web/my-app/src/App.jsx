import logo from './logo.svg';
import React, { Component } from 'react';
import './App.scss';
import Button from './Component/Button/Button';
import Modal from './Component/Modal/Modal';
import AllProduct from './Component/Product/AllProduct';

class App extends Component  {
  constructor(){
    super()
    this.state = {
      isShowFirstModal: false,
      isShowSecondModal: false,
      products: null,
    }
    
  }
  componentDidMount(){
    fetch('products.json')
    .then(response => response.json())
    .then(resolve => this.setState({products:resolve}))
  }

  closeFirstModal = () => {
    this.setState({isShowFirstModal: false})
  }
  closeSecondModal = () => {
    this.setState({isShowSecondModal: false})
  }

  showFirstModal = () => {
    this.setState({isShowFirstModal: true})
  }
  showSecondModal = () => {
    this.setState({isShowSecondModal: true})
  }
  
  render() {
    const footerFirstModal =
   <div>
      <Button text="OK" backgroundColor={"#a4261d"} functionClick={this.closeFirstModal}/>
      <Button text="Cancel" backgroundColor={"#a4261d"} functionClick={this.closeFirstModal}/>
    </div>
    const footerSecondModal = 
  <div>
    <Button text="OK" backgroundColor={"green"} functionClick={this.closeSecondModal}/>
    <Button text="Cancel" backgroundColor={"grey"} functionClick={this.closeSecondModal}/>
  </div>
const {isShowFirstModal, isShowSecondModal, products} = this.state;
  
    return (  
      <div>
        <div className="two-modal">
          <Button text="Open first modal" backgroundColor='red' functionClick={this.showFirstModal}/>
          <Button text="Open second modal" backgroundColor='blue' functionClick={this.showSecondModal}/>
        {isShowFirstModal && <Modal 
            functionClose={this.closeFirstModal}
            header="Do you want to delete this file?" 
            closeButton={true}
            textContent="Once you delete this file,it won't be possible to undo this action.
            Are you sure you want to delete it?"
            footer={footerFirstModal}
          />}
          {isShowSecondModal && <Modal 
            functionClose={this.closeSecondModal} 
            header="Second Modal"
            closeButton={true}
            textContent="content second modal"
            footer={footerSecondModal}
          />}
        </div>
            <AllProduct products={products} />

         
      </div>
      
    );
  }
  
}

export default App;
